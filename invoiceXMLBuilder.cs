using System;
using System.IO;
using System.Text;
using System.Collections.Generic;
using System.Data;
using Oracle.ManagedDataAccess.Client;
using System.Xml.Serialization;
using System.Linq;
using System.Net;
using System.Threading;
using System.Xml;
using System.Xml.Linq;

namespace oirs
{
    class InvoiceXMLBuilder
    {
        public static OracleConnectionStringBuilder builder = new OracleConnectionStringBuilder
        {
            DataSource = "192.168.98.37:1521/netapo",
            UserID = "netapo",
            Password = "netapo"
        };
        public static List<string> invoiceIdList;
        public static DataTable dtAllInvoices = new DataTable();

        public static SupplierInfoType CreateSupplierInfo()
        {
            var supplierTaxNumber = new TaxNumberType
            {
                vatCode = "2",
                taxpayerId = "26542993",
                countyCode = "41",
            };

            var supplierAddress = new AddressType
            {
                Item = new SimpleAddressType
                {
                    additionalAddressDetail = "Szabadföld út 79".ToUpper(),
                    city = "Budapest".ToUpper(),
                    countryCode = "HU".ToUpper(),
                    postalCode = "1164"
                }
            };
            var supplierInfo = new SupplierInfoType
            {
                supplierTaxNumber = supplierTaxNumber,
                supplierName = "Komtur Pharmaceuticals Hungary Kft".ToUpper(),
                supplierAddress = supplierAddress
            };
            return supplierInfo;
        }
        public static void GetInvoiceDataFromSql()
        {
            using (OracleConnection connection = new OracleConnection(builder.ConnectionString))
            {
                using (OracleCommand cmd = connection.CreateCommand())
                {
                    try
                    {
                        connection.Open();
                        cmd.BindByName = true;
                        var selectString = @"SELECT 
                                                nfr.BUCH_DATE as BUCH_DATE,
                                                nfr.BUCH_NR as BUCH_NR, 
                                                nfr.AEND_DATUM as LASTCHANGE, 
                                                nfr.KND_NAME1 as KND_NAME, 
                                                nfr.KNDA_Str as KND_STR, 
                                                nfr.KNDA_ZIP as KND_ZIP, 
                                                nfr.KNDA_ORT as KND_ORT,
                                                nfr.KNDA_LAND as KND_LAND,
                                                nfr.TAGESKURS as EXCHANGERATE,
                                                nfr.STATUSFLAG as INVOICESTATUS,
                                                nfrd.LANGNAME as ARTLANGNAME,
                                                nfrd.CHARGE as ARTCHARGE,
                                                nfrd.MENGE as ARTMENGE,
                                                nfrd.TAX as TAX,
                                                nfrd.NETTO as NETTO,
                                                nfrd.BRUTTO as BRUTTO,
                                                nk.USTID_LAND as USTID_LAND,
                                                nk.UST_ID as UST_ID,
                                                nw.FIBU_DESC as WAEHRUNG
                                            FROM NET_FB_RECHNUNG nfr,
                                                 NET_MAN2KND nmk, 
                                                 NET_KUNDEN nk, 
                                                 NET_FB_R_DETAIL nfrd, 
                                                 NET_WAEHRUNGEN nw 
                                            WHERE nmk.M2K_ID = nfr.MAN2KND_ID
                                            AND nfrd.FB_ID = nfr.FB_ID AND nmk.MAN_ID = 113 
                                            AND nk.KND_ID = nmk.KND_ID 
                                            AND nw.WAE_ID = nfr.WAE_ID
                                            ORDER BY 
                                                BUCH_NR,
                                                LASTCHANGE
                                                ";

                        cmd.CommandText = selectString;
                        using (OracleDataReader reader = cmd.ExecuteReader())
                        {
                            // Load the result of the SQL Query into a dataTable for further processing
                            dtAllInvoices.Load(reader);
                        }

                        //Retain the unique list of all known invoice ids 
                        invoiceIdList = dtAllInvoices
                        .AsEnumerable()
                        .Select(row => row.Field<string>("BUCH_NR"))
                        .Distinct()
                        .ToList();
                    }
                    catch (Exception ex)
                    {
                        ErrorLogging(ex);
                        throw ex;
                    }
                }
            }
        }
        public static void BuildAll()
        {

            var supplierInfo = CreateSupplierInfo();

            GetInvoiceDataFromSql();
            // InvoiceIdList is a static member that is filled when the data is retrieved from sql
            foreach (var invoiceId in invoiceIdList)
            {
                try
                {
                    // TODO: Add "fill default fields" method that fills fields that are allowed to be null in the db record
                    if (CheckInvoiceDataValid(invoiceId))
                    {
                        LogFail(invoiceId, "Invoice data is incomplete. Ignoring!");
                        continue;
                    }

                    // //Check if we have seen this invoice before
                    // if (CheckIfAlreadyProcessed(invoiceId))
                    // {
                    //     LogFail(invoiceId, "Invoice was already processed.");
                    //     continue;
                    // }

                    var invoiceBaseDatas = dtAllInvoices
                        .AsEnumerable()
                        .Where(row => row.Field<string>("BUCH_NR") == invoiceId)
                        .Select(row => new
                        {
                            invoiceId = row.Field<string>("BUCH_NR"),
                            invoiceDate = row.Field<DateTime>("BUCH_DATE"),
                            invoiceChangeDate = row.Field<DateTime>("LASTCHANGE"),
                            invoiceCustomerName = row.Field<string>("KND_NAME"),
                            invoiceCustomerStreet = row.Field<string>("KND_STR"),
                            invoiceCustomerZip = row.Field<string>("KND_ZIP"),
                            invoiceCustomerCity = row.Field<string>("KND_ORT"),
                            invoiceCustomerCountry = row.Field<string>("KND_LAND"),
                            invoiceCurrency = row.Field<string>("WAEHRUNG"),
                            //If exchange rate is set to 0, set to 1 instead to avoid problems
                            invoiceExchangeRate = row.Field<double>("EXCHANGERATE") < 0 || row.Field<double>("EXCHANGERATE") > 0 ? row.Field<double>("EXCHANGERATE") : 1,
                            invoiceAmount = Convert.ToDecimal(row.Field<double>("NETTO")),
                            invoiceVAT = Convert.ToDecimal(row.Field<double>("BRUTTO") - row.Field<double>("NETTO")),
                            invoiceStatus = row.Field<long>("INVOICESTATUS")
                        });
                    // the set is sorted by change date, so this is the most recent invoice with this nr.
                    var invoiceBaseData = invoiceBaseDatas.First();

                    if (invoiceBaseData.invoiceStatus <= 4 || invoiceBaseData.invoiceStatus >= 7)
                    {
                        LogFail(invoiceId, $"Invoice was skipped as it ahs status flag {invoiceBaseData.invoiceStatus}. Ignoring!");
                        continue;

                    }


                    var invoiceTotal = Convert.ToDecimal(dtAllInvoices
                        .AsEnumerable()
                        .Where(row => row.Field<string>("BUCH_NR") == invoiceId)
                        .Sum(row => row.Field<double>("NETTO")));

                    var invoiceTotalBrutto = Convert.ToDecimal(dtAllInvoices
                    .AsEnumerable()
                    .Where(row => row.Field<string>("BUCH_NR") == invoiceId)
                    .Sum(row => row.Field<double>("BRUTTO")));

                    var allInvoiceLines = dtAllInvoices
                    .AsEnumerable()
                    .Where(row => row.Field<string>("BUCH_NR") == invoiceId)
                    .Select((row, rowNumber) => new LineType
                    {
                        lineDescription = row.Field<string>("ARTLANGNAME") + " " + row.Field<string>("ARTCHARGE"),
                        lineNatureIndicator = LineNatureIndicatorType.PRODUCT,
                        lineExpressionIndicator = false,
                        lineNumber = (rowNumber + 1).ToString(),
                    });

                    var invoiceLineSummaries = dtAllInvoices
                    .AsEnumerable()
                    .Where(row => row.Field<string>("BUCH_NR") == invoiceId)
                    .Select(row => new SummaryByVatRateType
                    {
                        vatRate = new VatRateType { Item = row.Field<decimal>("TAX") / 100, ItemElementName = ItemChoiceType2.vatPercentage },
                        vatRateNetData = new VatRateNetDataType
                        {
                            vatRateNetAmount = Decimal.Round(Convert.ToDecimal(row.Field<double>("NETTO")), 2),
                            vatRateNetAmountHUF = Decimal.Round(Convert.ToDecimal(row.Field<double>("NETTO") * invoiceBaseData.invoiceExchangeRate), 2),
                        },
                        vatRateVatData = new VatRateVatDataType
                        {
                            vatRateVatAmount = Decimal.Round(Convert.ToDecimal(row.Field<double>("BRUTTO") - row.Field<double>("NETTO")), 2),
                            vatRateVatAmountHUF = Decimal.Round(Convert.ToDecimal((row.Field<double>("BRUTTO") - row.Field<double>("NETTO")) * invoiceBaseData.invoiceExchangeRate), 2)
                        }
                    });

                    var customerAddress = new AddressType
                    {
                        Item = new SimpleAddressType
                        {
                            additionalAddressDetail = invoiceBaseData.invoiceCustomerStreet.ToUpper(),
                            city = invoiceBaseData.invoiceCustomerCity.ToUpper(),
                            postalCode = invoiceBaseData.invoiceCustomerZip,
                            countryCode = invoiceBaseData.invoiceCustomerCountry.ToUpper()
                        }
                    };
                    var customerInfo = new CustomerInfoType
                    {
                        customerName = invoiceBaseData.invoiceCustomerName.ToUpper(),
                        customerAddress = customerAddress
                    };

                    var invoiceExchangeRate = Decimal.Round(Convert.ToDecimal(invoiceBaseData.invoiceExchangeRate), 6);
                    var invoiceDetail = new InvoiceDetailType
                    {
                        invoiceCategory = InvoiceCategoryType.NORMAL,
                        invoiceDeliveryDate = invoiceBaseData.invoiceDate,
                        currencyCode = invoiceBaseData.invoiceCurrency,
                        exchangeRate = invoiceExchangeRate,
                        invoiceAppearance = InvoiceAppearanceType.UNKNOWN,
                    };
                    var invoiceHead = new InvoiceHeadType
                    {
                        supplierInfo = supplierInfo,
                        invoiceDetail = invoiceDetail,
                        customerInfo = customerInfo
                    };


                    var invoiceSummaryNormal = new SummaryNormalType[] {
                                new SummaryNormalType {
                                    summaryByVatRate = invoiceLineSummaries.ToArray(),
                                    invoiceNetAmount = Decimal.Round(invoiceTotal, 2),
                                    invoiceVatAmount = Decimal.Round(invoiceTotalBrutto - invoiceTotal, 2),
                                    invoiceNetAmountHUF = Decimal.Round(invoiceTotal * invoiceExchangeRate, 2),
                                    invoiceVatAmountHUF = Decimal.Round((invoiceTotalBrutto - invoiceTotal) * invoiceExchangeRate, 2)
                                    }
                                };

                    var invoiceSummary = new SummaryType
                    {
                        Items = invoiceSummaryNormal
                    };

                    var invoice = new InvoiceType
                    {
                        invoiceHead = invoiceHead,
                        invoiceLines = new LinesType { line = allInvoiceLines.ToArray() },
                        invoiceSummary = invoiceSummary,
                    };
                    var invoiceArray = new InvoiceType[] { invoice };

                    var invoiceMain = new InvoiceMainType
                    {
                        Items = invoiceArray,
                    };

                    var invoiceData = new InvoiceData
                    {
                        completenessIndicator = true,
                        invoiceIssueDate = invoiceBaseData.invoiceDate,
                        invoiceNumber = invoiceId,
                        invoiceMain = invoiceMain
                    };
                    //Debug prints!
                    Console.WriteLine("Items unter RechnungsID " + invoiceId);
                    foreach (var item in allInvoiceLines)
                    {
                        Console.WriteLine(item.lineDescription);
                    }
                    //Create the XML!
                    var serializer = new XmlSerializer(typeof(InvoiceData));
                    XmlSerializerNamespaces ns = new XmlSerializerNamespaces();
                    ns.Add("base", "http://schemas.nav.gov.hu/OSA/3.0/base");
                    //using (var stream = new StreamWriter(@".\XML\" + invoiceId + ".xml"))
                    using (var stream = new MemoryStream())
                    {

                        serializer.Serialize(stream, invoiceData, ns);

                        //Add a comment to the XML root node if the status is storno
                        if (invoiceBaseData.invoiceStatus == 6)
                        {
                            //Jump back to the start of the stream
                            stream.Seek(0, 0);
                            XDocument doc = XDocument.Load(stream);
                            stream.Seek(0, 0);
                            XElement node = doc.Root;
                            XComment comm = new XComment("<invoiceOperation>STORNO</invoiceOperation>");
                            node.Add(comm);
                            doc.Save(stream);
                        }
                        using (FileStream file = new FileStream(@".\XML\" + invoiceId + ".xml", FileMode.OpenOrCreate, FileAccess.Write))
                        {
                            // discard the contents of the file by setting the length to 0
                            file.SetLength(0);
                            stream.WriteTo(file);
                        }
                    }
                    LogSuccess(invoiceId);
                    // AddToProcessedList(invoiceId);
                }

                catch (Exception ex)
                {
                ErrorLogging(ex);
                    //throw ex;
                }
            }

        }





        private static bool CheckInvoiceDataValid(string invoiceId)
        {
            bool valid = true;

            // Check if any of the required fields is null, and if it is, abort! (Invoice Incomplete)
            foreach (DataColumn column in dtAllInvoices.Columns)
            {
                if (dtAllInvoices.AsEnumerable()
                                 .Where(row => row.Field<string>("BUCH_NR") == invoiceId)
                                 .Any(row => row.IsNull(column)))
                {
                    valid = false;
                }
            }

            return valid;
        }

        public static void ErrorLogging(Exception ex)
        {
            string strPath = @".\Logs\CrashLog.txt";
            if (!File.Exists(strPath))
            {
                File.Create(strPath).Dispose();
            }
            using (StreamWriter sw = File.AppendText(strPath))
            {

                sw.WriteLine("===========NEW ERROR============= " + DateTime.Now);
                sw.WriteLine("Error Message: " + ex.Message);
                sw.WriteLine("Stack Trace: " + ex.StackTrace);
                sw.WriteLine("===========End============= " + DateTime.Now);

            }
        }
        public static void LogSuccess(string invoiceId)
        {
            string strPath = @".\Logs\GenerationLog.txt";
            if (!File.Exists(strPath))
            {
                File.Create(strPath).Dispose();
            }
            using (StreamWriter w = File.AppendText(strPath))
            {
                w.WriteLine($"{DateTime.Now}: Successfully exported :{invoiceId} to xml");
            }

        }
        public static void LogFail(string invoiceId, string reason)
        {
            string strPath = @".\Logs\ErrorLog.txt";
            if (!File.Exists(strPath))
            {
                File.Create(strPath).Dispose();
            }
            using (StreamWriter w = File.AppendText(strPath))
            {
                w.WriteLine($"{DateTime.Now}: {invoiceId} skipped! {reason}");
            }

        }
        // public static void AddToProcessedList(string invoiceId)
        // {
        //     string strPath = @".\Logs\knownInvoices.txt";
        //     if (!File.Exists(strPath))
        //     {
        //         File.Create(strPath).Dispose();
        //     }
        //     using (StreamWriter sw = File.AppendText(strPath))
        //     {
        //         sw.WriteLine($"{DateTime.Now}: {invoiceId}");

        //     }
        // }
        // public static bool CheckIfAlreadyProcessed(string invoiceId)
        // {
        //     string strPath = @".\Logs\knownInvoices.txt";
        //     if (!File.Exists(strPath))
        //     {
        //         File.Create(strPath).Dispose();
        //     }
        //     if (File.ReadAllText(strPath).Contains(invoiceId))
        //     {
        //         return true;
        //     }
        //     return false;
        // }
    }


}
